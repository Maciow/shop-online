package com.shoponline.service;

import com.shoponline.model.Product;
import com.shoponline.model.Vote;
import com.shoponline.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class ProductService {

    private ProductRepository productRepository;

    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Page<Product> findPaginatedProducts(int page, int size, String category){
        return productRepository.findByCategory(PageRequest.of(page,size),category);
    }

    public Product findById(Long productId){
        return productRepository.findById(productId).get();
    }

    public void saveProductsFromSet(Set<Product> products){
        for (Product p: products){
            productRepository.save(p);
        }
        productRepository.flush();
    }

    public void updateRating(Product product){
        Double average = product.getVotes().stream().mapToDouble(Vote::getVoteValue).average().orElse(0.0);
        productRepository.updateRating(average,product.getId());
    }

    public void save(Product product){
        productRepository.save(product);
    }
}
