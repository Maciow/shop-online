package com.shoponline.service;

import com.shoponline.exception.NotEnoughProductsInStockException;
import com.shoponline.model.Product;
import com.shoponline.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Transactional
public class ShoppingCartService {

    private ProductRepository productRepository;
    private ProductService productService;

    private Map<Product, Integer> products = new HashMap<>();

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public void addToCart(Product product){
        if (products.containsKey(product)){
            products.replace(product, products.get(product)+1);
        } else {
            products.put(product,1);
        }
    }

    public void removeFromCart(Product product){
        if (products.containsKey(product)){
            if (products.get(product)>1){
                products.replace(product,products.get(product)-1);
            } else if (products.get(product)==1){
                products.remove(product);
            }
        }
    }

    public Map<Product, Integer> getProductsInCart(){
        return Collections.unmodifiableMap(products);
    }

    public void checkout() throws NotEnoughProductsInStockException {
        Product product;
        for (Map.Entry<Product, Integer> entry: products.entrySet()){
            product = productRepository.findById(entry.getKey().getId()).get();
            if (product.getQuantity() < entry.getValue()){
                throw new NotEnoughProductsInStockException(product);
            }
            entry.getKey().setQuantity(product.getQuantity()-entry.getValue());
        }
        productService.saveProductsFromSet(products.keySet());
        products.clear();
    }

    public void preCheckout() throws NotEnoughProductsInStockException {
        Product product;
        for (Map.Entry<Product, Integer> entry: products.entrySet()){
            product = productRepository.findById(entry.getKey().getId()).get();
            if (product.getQuantity() < entry.getValue()){
                throw new NotEnoughProductsInStockException(product);
            }
        }
    }

    public BigDecimal getTotal(){
        return products.entrySet().stream()
                .map(entry->entry.getKey().getNewPrice().multiply(BigDecimal.valueOf(entry.getValue())))
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
    }
}
