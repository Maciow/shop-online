package com.shoponline.service;

import com.shoponline.model.Product;
import com.shoponline.model.User;
import com.shoponline.model.Vote;
import com.shoponline.repository.VoteRepository;
import com.shoponline.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;

@Service
public class VoteService {

    private VoteRepository voteRepository;
    private ProductService productService;
    private UserService userService;

    @Autowired
    public void setVoteRepository(VoteRepository voteRepository) {
        this.voteRepository = voteRepository;
    }

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }
    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void addVoteToProduct(Integer voteValue, Long productId){
        User user = userService.findByEmail(UserUtils.getLoggedEmail());
        Product product = productService.findById(productId);
        Vote voteFromRepo = voteRepository.findByUserAndAndProduct(user,product);
        if (voteFromRepo==null) {
            createAndAddVoteToProduct(voteValue, user, product);
        } else {
            product.getVotes().remove(voteFromRepo);
            voteRepository.deleteVoteById(voteFromRepo.getId());
            createAndAddVoteToProduct(voteValue,user,product);
        }
    }

    private void createAndAddVoteToProduct(Integer voteValue, User user, Product product) {
        Vote vote = new Vote();
        vote.setVoteValue(voteValue);
        vote.setProduct(product);
        vote.setTimestamp(Timestamp.from(Instant.now()));
        vote.setUser(user);
        product.getVotes().add(vote);
        voteRepository.save(vote);
        productService.save(product);
        productService.updateRating(product);
    }
}
