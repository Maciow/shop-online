package com.shoponline.service;

import com.shoponline.email.sender.EmailSender;
import com.shoponline.model.User;
import com.shoponline.repository.UserRepository;
import com.shoponline.utils.ApplicationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private static final String DEFAULT_ROLE = "ROLE_USER";
    private PasswordEncoder passwordEncoder;
    private UserRepository userRepository;
    private UserRoleService roleService;
    private EmailSender emailSender;

    @Autowired
    public void setEmailSender(EmailSender emailSender) {
        this.emailSender = emailSender;
    }

    @Autowired
    public UserService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setRoleService(UserRoleService roleService) {
        this.roleService = roleService;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User findByUsername(String username){
        return userRepository.findByUsername(username);
    }

    public User findByEmail(String email){
        return userRepository.findByEmail(email);
    }

    public void addUserWithDefaultRole(User user){
        user.setActivationCode(ApplicationUtils.randomStringGenerator());
        user.getRoles().add(roleService.findByRole(DEFAULT_ROLE));
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setConfirmed(false);
        userRepository.save(user);
        sendEmail(user,emailSender);
    }

    private static void sendEmail(User user, EmailSender emailSender){
        String content = "Open the link to confirm your account \n. http://localhost:8080/activatelink/"+user.getActivationCode();
        emailSender.sendEmail(user.getEmail(), "Register confirmation", content);
    }

    public void updateConfirmation(Boolean isConfirmed, String activationCode){
        userRepository.updateConfirmation(isConfirmed,activationCode);
    }
}
