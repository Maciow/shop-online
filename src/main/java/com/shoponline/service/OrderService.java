package com.shoponline.service;

import com.shoponline.model.Order;
import com.shoponline.model.OrderState;
import com.shoponline.model.Product;
import com.shoponline.repository.OrderRepository;
import com.shoponline.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Map;

@Service
public class OrderService {

    private OrderRepository orderRepository;
    private UserService userService;
    private ShoppingCartService shoppingCartService;

    @Autowired
    public void setShoppingCartService(ShoppingCartService shoppingCartService) {
        this.shoppingCartService = shoppingCartService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setOrderRepository(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public void addOrderWithDetails(Order order) {
        order.setUser(userService.findByEmail(UserUtils.getLoggedEmail()));
        order.setDate(Timestamp.from(Instant.now()));
        order.setOrderState(OrderState.PENDING);
        putDetailsToOrderMap(order);
        orderRepository.save(order);
    }

    private void putDetailsToOrderMap(Order order){
        for (Map.Entry<Product,Integer> entry: shoppingCartService.getProductsInCart().entrySet()){
            order.getProducts().put(entry.getKey().getId(),entry.getValue());
        }
    }
}
