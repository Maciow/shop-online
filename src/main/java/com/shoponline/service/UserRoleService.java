package com.shoponline.service;

import com.shoponline.model.UserRole;
import com.shoponline.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRoleService {

    private UserRoleRepository roleRepository;

    @Autowired
    public void setRoleRepository(UserRoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public UserRole findByRole(String role){
        return roleRepository.findByRole(role);
    }
}
