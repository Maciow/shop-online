package com.shoponline.service;

import com.shoponline.model.Comment;
import com.shoponline.model.Product;
import com.shoponline.repository.CommentRepository;
import com.shoponline.repository.ProductRepository;
import com.shoponline.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;

@Service
public class CommentService {

    private CommentRepository commentRepository;
    private UserService userService;
    private ProductRepository productRepository;

    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setCommentRepository(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    public void addCommentWithDetails(Comment comment,Product product){
        comment.setProduct(product);
        comment.setTimestamp(Timestamp.from(Instant.now()));
        comment.setUser(userService.findByEmail(UserUtils.getLoggedEmail()));
        Comment newComment = new Comment(comment);
        commentRepository.save(newComment);
    }
}
