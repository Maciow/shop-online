package com.shoponline.email.sender;

public interface EmailSender {
    void sendEmail(String to, String subject, String content);
}
