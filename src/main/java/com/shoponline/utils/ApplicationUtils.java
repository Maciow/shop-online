package com.shoponline.utils;

import java.util.Random;

public class ApplicationUtils {

    public static String randomStringGenerator(){
        StringBuilder randomString = new StringBuilder();

        String signs = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

        Random random = new Random();

        for (int i=0; i<32; i++){
            int number = random.nextInt(signs.length());
            randomString.append(signs, number, number + 1);
        }
        return randomString.toString();
    }
}
