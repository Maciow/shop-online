package com.shoponline.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Entity
@Table(name = "orders")
public class Order implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_order")
    private Long id;
    @ElementCollection
    @CollectionTable(name = "product_quantity",
            joinColumns = {@JoinColumn(name = "order_id")})
    @MapKeyColumn(name = "product")
    @Column(name = "quantity")
    private Map<Long,Integer> products = new HashMap<>();
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private User user;
    private Timestamp date;
    @Column(name = "order_state")
    @Enumerated(EnumType.STRING)
    private OrderState orderState;
    @NotEmpty(message = "{com.shoponline.model.Order.firstname.NotEmpty}")
    @Column(name = "customer_first_name")
    private String customerFirstName;
    @NotEmpty(message = "{com.shoponline.model.Order.lastname.NotEmpty}")
    @Column(name = "customer_last_name")
    private String customerLastName;
    @NotEmpty(message = "{com.shoponline.model.Order.country.NotEmpty}")
    @Column(name = "customer_country")
    private String customerCountry;
    @NotEmpty(message = "{com.shoponline.model.Order.city.NotEmpty}")
    @Column(name = "customer_city")
    private String customerCity;
    @NotEmpty(message = "{com.shoponline.model.Order.street.NotEmpty}")
    @Column(name = "customer_street")
    private String customerStreet;
    @NotEmpty(message = "{com.shoponline.model.Order.postalCode.NotEmpty}")
    @Column(name = "customer_postal_code")
    private String customerPostalCode;

    public Order() {
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", types of products=" + products.size() +
                ", user ID=" + user.getId() +
                ", date=" + date +
                ", orderState=" + orderState +
                ", customerFirstName='" + customerFirstName + '\'' +
                ", customerLastName='" + customerLastName + '\'' +
                ", customerCountry='" + customerCountry + '\'' +
                ", customerCity='" + customerCity + '\'' +
                ", customerStreet='" + customerStreet + '\'' +
                ", customerPostalCode='" + customerPostalCode + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equals(id, order.id) &&
                Objects.equals(products, order.products) &&
                Objects.equals(user, order.user) &&
                Objects.equals(date, order.date) &&
                orderState == order.orderState &&
                Objects.equals(customerFirstName, order.customerFirstName) &&
                Objects.equals(customerLastName, order.customerLastName) &&
                Objects.equals(customerCountry, order.customerCountry) &&
                Objects.equals(customerCity, order.customerCity) &&
                Objects.equals(customerStreet, order.customerStreet) &&
                Objects.equals(customerPostalCode, order.customerPostalCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, products, user, date, orderState, customerFirstName, customerLastName, customerCountry, customerCity, customerStreet, customerPostalCode);
    }

    public Map<Long, Integer> getProducts() {
        return products;
    }

    public void setProducts(Map<Long, Integer> products) {
        this.products = products;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public OrderState getOrderState() {
        return orderState;
    }

    public void setOrderState(OrderState orderState) {
        this.orderState = orderState;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    public String getCustomerCountry() {
        return customerCountry;
    }

    public void setCustomerCountry(String customerCountry) {
        this.customerCountry = customerCountry;
    }

    public String getCustomerCity() {
        return customerCity;
    }

    public void setCustomerCity(String customerCity) {
        this.customerCity = customerCity;
    }

    public String getCustomerStreet() {
        return customerStreet;
    }

    public void setCustomerStreet(String customerStreet) {
        this.customerStreet = customerStreet;
    }

    public String getCustomerPostalCode() {
        return customerPostalCode;
    }

    public void setCustomerPostalCode(String customerPostalCode) {
        this.customerPostalCode = customerPostalCode;
    }
}
