package com.shoponline.model;

public enum OrderState {
    PENDING("Waiting for approval"),
    APPROVED("Order accepted"),
    ON_THE_WAY("Order is on the way"),
    DELIVERED("Order has been delivered");

    public String description;

    OrderState(String description) {
        this.description = description;
    }
}
