package com.shoponline.validator;

import com.shoponline.model.User;
import org.springframework.validation.Errors;

public class UserRegistrationValidator {

    public void validateUsernameExists(User user, Errors errors){
        if (user!=null){
            errors.rejectValue("username","error.email.username");
        }
    }

    public void validateEmailExists(User user, Errors errors){
        if (user!=null){
            errors.rejectValue("email","error.email.exists");
        }
    }

    public void validatePasswordLength(String password, Errors errors){
        if (password.length()>20){
            errors.rejectValue("password","com.shoponline.model.User.password.Size");
        }
    }

    public void validateConfirmPassword(User user, Errors errors){
        String password = user.getPassword();
        String confirmPassword = user.getConfirmPassword();
        if (!password.equals(confirmPassword)){
            errors.rejectValue("confirmPassword","error.password.confirm");
        }
    }
}
