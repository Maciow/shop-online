package com.shoponline.controller;

import com.shoponline.service.VoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class VoteController {

    private VoteService voteService;

    @Autowired
    public VoteController(VoteService voteService) {
        this.voteService = voteService;
    }

    @PostMapping("/products/{id}/vote")
    public String addVoteToProduct(final Integer voteValue, @PathVariable Long id){
        System.out.println(voteValue);
        voteService.addVoteToProduct(voteValue,id);
        return "redirect:/products/"+id;
    }
}
