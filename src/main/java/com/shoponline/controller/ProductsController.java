package com.shoponline.controller;

import com.shoponline.exception.ResourceNotFoundException;
import com.shoponline.model.Comment;
import com.shoponline.model.Product;
import com.shoponline.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/products")
public class ProductsController {

    private ProductService productService;

    @Autowired
    public ProductsController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(
            value = "/{category}",
            params = {"page","size"}
    )
    public String findPaginatedProducts(@RequestParam int page,
                                               @RequestParam int size,
                                               @PathVariable String category,
                                               Model model){
        Page<Product> resultPage = productService.findPaginatedProducts(page-1,size,category);
        List<Product> productList = resultPage.getContent();
        model.addAttribute("totalPages", resultPage.getTotalPages());
        model.addAttribute("currentPage", resultPage.getNumber()+1);
        model.addAttribute("productList", productList);
        model.addAttribute("size", size);
        model.addAttribute("category", category);
        return "category";
    }

    @GetMapping("/{id}")
    public String productDetails(@PathVariable Long id, Model model){
        Product product = productService.findById(id);
        model.addAttribute("product", product);
        if (!model.containsAttribute("comment")) {
            model.addAttribute("comment", new Comment());
        }
        model.addAttribute("commentsList", product.getComments());
        return "productDetails";
    }

}
