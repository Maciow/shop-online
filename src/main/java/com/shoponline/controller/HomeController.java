package com.shoponline.controller;

import com.shoponline.utils.UserUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class HomeController {

    @GetMapping("/")
    public String getHomePage(){
        return "index";
    }

    @GetMapping("/login")
    public String getLoginForm(){
        String username = UserUtils.getLoggedEmail();
        if (username.equals("anonymousUser")){
            return "login";
        } else return "redirect:/";
    }
}
