package com.shoponline.controller;

import com.shoponline.captcha.service.CaptchaService;
import com.shoponline.model.User;
import com.shoponline.service.UserService;
import com.shoponline.validator.UserRegistrationValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class UserController {

    private UserService userService;
    private CaptchaService captchaService;

    @Autowired
    public UserController(UserService userService, CaptchaService captchaService) {
        this.userService = userService;
        this.captchaService = captchaService;
    }

    @GetMapping("/register")
    public String getRegisterPage(Model model){
        model.addAttribute("user",new User());
        return "register";
    }

    @PostMapping("/register")
    public String addUser(@Valid @ModelAttribute User user, BindingResult result, HttpServletRequest request){
        String response = request.getParameter("g-recaptcha-response");
        captchaService.processResponse(response,result);

        new UserRegistrationValidator().validateUsernameExists(userService.findByUsername(user.getUsername()),result);
        new UserRegistrationValidator().validateEmailExists(userService.findByEmail(user.getEmail()),result);
        new UserRegistrationValidator().validateConfirmPassword(user,result);
        new UserRegistrationValidator().validatePasswordLength(user.getPassword(),result);

        if (result.hasErrors()){
            return "register";
        } else {
            userService.addUserWithDefaultRole(user);
            //should be a successful registration page
            return "redirect:/";
        }
    }

    @GetMapping("/activatelink/{activationCode}")
    public String activateAccount(@PathVariable String activationCode){
        userService.updateConfirmation(true,activationCode);
        return "successfulRegister";
    }
}
