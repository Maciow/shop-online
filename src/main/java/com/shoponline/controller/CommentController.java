package com.shoponline.controller;

import com.shoponline.model.Comment;
import com.shoponline.model.Product;
import com.shoponline.service.CommentService;
import com.shoponline.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class CommentController {

    private CommentService commentService;
    private ProductService productService;

    @Autowired
    public CommentController(CommentService commentService, ProductService productService) {
        this.commentService = commentService;
        this.productService = productService;
    }

    @PostMapping("/products/{id}/comment")
    public String addCommentToProduct(@Valid @ModelAttribute Comment comment,
                                      BindingResult result,
                                      @PathVariable Long id,
                                      RedirectAttributes attributes){
        if (result.hasErrors()){
            attributes.addFlashAttribute("org.springframework.validation.BindingResult.comment", result);
            attributes.addFlashAttribute("comment", comment);
            return "redirect:/products/"+id;
        } else {
            commentService.addCommentWithDetails(comment, productService.findById(id));
        }
        return "redirect:/products/"+id;
    }
}
