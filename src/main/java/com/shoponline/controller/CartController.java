package com.shoponline.controller;

import com.shoponline.exception.NotEnoughProductsInStockException;
import com.shoponline.model.Order;
import com.shoponline.model.Product;
import com.shoponline.model.User;
import com.shoponline.service.OrderService;
import com.shoponline.service.ProductService;
import com.shoponline.service.ShoppingCartService;
import com.shoponline.service.UserService;
import com.shoponline.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class CartController {

    private ProductService productService;
    private ShoppingCartService shoppingCartService;
    private UserService userService;
    private OrderService orderService;

    @Autowired
    public CartController(ProductService productService, ShoppingCartService shoppingCartService, UserService userService, OrderService orderService) {
        this.productService = productService;
        this.shoppingCartService = shoppingCartService;
        this.userService = userService;
        this.orderService = orderService;
    }

    @PostMapping("/add-to-cart")
    public String addToCart(final Long productId,
                            HttpServletRequest request){
        Product product = productService.findById(productId);
        if (product!=null) {
            shoppingCartService.addToCart(product);
        }
        String referer = request.getHeader("Referer");
        return "redirect:"+referer;
    }

    @PostMapping("/remove-from-cart")
    public String removeFromCart(final Long productId){
        shoppingCartService.removeFromCart(productService.findById(productId));
        return "redirect:/cart";
    }

    @GetMapping("/cart")
    public String viewCartDetails(Model model, @ModelAttribute String productsException){
        model.addAttribute("products", shoppingCartService.getProductsInCart());
        model.addAttribute("total", shoppingCartService.getTotal());
        if (!productsException.equals("")) {
            model.addAttribute("productsException", productsException);
        }
        return "cart";
    }

    @GetMapping("/cart/shopping-details")
    public String shoppingDetails(Model model){
        String email = UserUtils.getLoggedEmail();
        if (!email.equals("anonymousUser")){
            User user = userService.findByEmail(email);
            if (user.getUserDetails()!=null){
                model.addAttribute("userDetails", user.getUserDetails());
            }
        }
        model.addAttribute("order", new Order());
        return "shoppingDetails";
    }

    @PostMapping("/cart/check-shopping-details")
    public String checkShoppingDetails(@Valid @ModelAttribute Order order, BindingResult result, RedirectAttributes attributes){
        try {
            shoppingCartService.preCheckout();
        } catch (NotEnoughProductsInStockException e) {
            attributes.addFlashAttribute("productsException", e.getMessage());
            return "redirect:/cart";
        }
        if (result.hasErrors()){
            return "shoppingDetails";
        } else {
            orderService.addOrderWithDetails(order);
            return "redirect:/cart/checkout";
        }
    }

    @GetMapping("/cart/checkout")
    public String checkout(RedirectAttributes attributes){
        try {
            shoppingCartService.checkout();
        } catch (NotEnoughProductsInStockException e) {
            attributes.addFlashAttribute("productsException", e.getMessage());
            return "redirect:/cart";
        }
        //should be a successful order page with details about order
        return "redirect:/";
    }

}
