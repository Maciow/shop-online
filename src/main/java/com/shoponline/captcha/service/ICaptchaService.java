package com.shoponline.captcha.service;

import org.springframework.validation.Errors;

public interface ICaptchaService {
    void processResponse(final String response, Errors errors);

    String getReCaptchaSite();

    String getReCaptchaSecret();
}
