package com.shoponline.repository;

import com.shoponline.model.Product;
import com.shoponline.model.User;
import com.shoponline.model.Vote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;

@Repository
@Transactional
public interface VoteRepository extends JpaRepository<Vote, Long> {
    Vote findByUserAndAndProduct(User user, Product product);

    void deleteVoteById(Long voteId);
}
