package com.shoponline.repository;

import com.shoponline.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);
    User findByUsername(String username);

    @Modifying
    @Query("UPDATE User u SET u.isConfirmed=:isConfirmed WHERE u.activationCode=:activationCode")
    void updateConfirmation(@Param("isConfirmed")Boolean isConfirmed, @Param("activationCode") String activationCode);
}
