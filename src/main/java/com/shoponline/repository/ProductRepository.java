package com.shoponline.repository;

import com.shoponline.model.Comment;
import com.shoponline.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface ProductRepository extends JpaRepository<Product, Long> {
    @Query("SELECT p FROM Product p WHERE p.category=:category")
    Page<Product> findByCategory(Pageable pageable, @Param("category") String category);

    @Modifying
    @Query("UPDATE Product p SET p.rating=:rating WHERE p.id=:id")
    void updateRating(@Param("rating") Double rating, @Param("id") Long id);

    Optional<Product> findById(Long productId);
}
